﻿namespace Atlassian.Jira.OAuth.CLI
{
    class TokenResults
    {
        public TokenResults(string consumerKey, string privateKey, OAuthRequestToken requestToken, string accessToken)
        {
            ConsumerKey = consumerKey;
            PrivateKey = privateKey;
            RequestToken = requestToken.OAuthToken;
            TokenSecret = requestToken.OAuthTokenSecret;
            AccessToken = accessToken;
        }

        public string ConsumerKey { get; set; }
        public string PrivateKey { get; set; }
        public string RequestToken { get; set; }
        public string AccessToken { get; set; }
        public string TokenSecret { get; set; }
        public string SampleCode
        {
            get
            {
                return $@"
var consumerKey = ""{ ConsumerKey }"";
var consumerSecret = ""{PrivateKey}"";
var accessToken = ""{ AccessToken}"";
var tokenSecret = ""{TokenSecret}"";

var jiraWithOAuth = await Jira.CreateOAuthRestClientAsync(
    ""http://my.jira.url""
    consumerKey,
    consumerSecret,
    accessToken,
    tokenSecret);";
            }
        }
    }
}
