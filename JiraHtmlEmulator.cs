﻿using LTAF;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atlassian.Jira.OAuth.CLI
{
    public class JiraHtmlEmulator
    {
        private readonly HtmlPage page;

        public JiraHtmlEmulator(string url)
        {
            var basePath = new Uri(url);
            page = new HtmlPage(basePath);

            var browserEmulator = new LTAF.Emulators.BrowserEmulator(basePath.AbsoluteUri);
            browserEmulator.SilentMode = true;
            page.BrowserCommandExecutor = browserEmulator.CreateCommandExecutor();
        }

        public JiraHtmlEmulator Login(string user, string password)
        {
            page.Navigate("/login.jsp");

            var elements = page.RootElement.ChildElements;
            elements.Find("login-form-username", MatchMethod.Literal).SetText(user);
            elements.Find("login-form-password", MatchMethod.Literal).SetText(password);
            elements.Find("login-form-submit", MatchMethod.Literal).Click();

            return this;
        }

        public JiraHtmlEmulator Authorize(string authorizeUrl)
        {
            page.Navigate(authorizeUrl);
            page.RootElement.ChildElements.Find("approve", MatchMethod.Literal).Click();
            return this;
        }
    }
}
