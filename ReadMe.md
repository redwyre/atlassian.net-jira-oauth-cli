﻿# JIRA OAuth CLI

Command line tool that helps developers setup OAuth in Jira and retrieve tokens to be used with the [Atlassian .NET SDK](https://bitbucket.org/farmas/atlassian.net-sdk).

## Requirements

- .NET Core.
- Access to a Jira server v7 or above.
- Windows OS.

## Install

```
dotnet tool install -g jira-oauth-cli
```

## Use

### Generate Public/Private Keys
Will generate and print a key pair that can be used when configuring JIRA's app link with OAuth. The private key is printed in XML format which can be used as an input parameter for other methods.

```
jira-oauth-cli get-keys
```

### Generate Tokens
Generates a request and access token that can be used to authenticate with Jira.

```
jira-oauth-cli get-tokens --url http://localhost:8080 -u username -p password -k YourConsumerKey -s YourPrivateKey
```

### Manual Setup
Guides you through the setup of an app link using JIRA's web UI and generates the request and access tokens that can be used to authenticate with Jira.
```
jira-oauth-cli setup --url http://localhost:8080 -u username -p password -k YourConsumerKey
```